"""
Resources related to our connection panel.
"""

__all__ = [
  'circ_entry',
  'conn_entry',
  'conn_panel',
  'descriptor_popup',
  'entries',
]
